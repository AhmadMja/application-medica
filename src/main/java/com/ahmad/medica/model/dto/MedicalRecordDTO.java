package com.ahmad.medica.model.dto;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MedicalRecordDTO {

    private Long id;

    private String code;

    private String source;

    private String codeListCode;

    private String displayValue;

    private String longDescription;

    private LocalDate fromDate;
    private LocalDate toDate;

    private Integer sortingPriority;
}
