package com.ahmad.medica.model.entity;

import com.ahmad.medica.model.dto.MedicalRecordDTO;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * @implNote I assumed that sorting_priority is a highly used column. So I've added an index to increase database search speed based on this field.
 */
@Entity
@Table(name = "medical_record", indexes = @Index(columnList = "sortingPriority"))
@Getter
@Setter
public class MedicalRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String code;

    private String source;

    private String codeListCode;

    private String displayValue;

    @Lob
    @Column(length = 1024)
    private String longDescription;

    private LocalDate fromDate;

    private LocalDate toDate;

    private Integer sortingPriority;

    public MedicalRecordDTO toDto() {
        return new MedicalRecordDTO(id, code, source, codeListCode, displayValue, longDescription,
            fromDate, toDate, sortingPriority);
    }
}
