package com.ahmad.medica.model.repository;

import com.ahmad.medica.model.entity.MedicalRecord;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicalRecordRepository extends JpaRepository<MedicalRecord, Long> {

    Optional<MedicalRecord> findByCode(String code);

    Page<MedicalRecord> findAll(Pageable pageable);
}
