package com.ahmad.medica.controller;

import com.ahmad.medica.model.dto.MedicalRecordDTO;
import com.ahmad.medica.model.dto.ResponseDTO;
import com.ahmad.medica.model.entity.MedicalRecord;
import com.ahmad.medica.model.exception.FileFormatException;
import com.ahmad.medica.model.exception.MedicalRecordNotFoundException;
import com.ahmad.medica.service.FileProcessorService;
import com.ahmad.medica.service.MedicalRecordService;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/data/medical-record")
public class MedicalRecordController {

    private final MedicalRecordService medicalRecordService;
    private final FileProcessorService fileProcessorService;

    @Autowired
    public MedicalRecordController(MedicalRecordService medicalRecordService,
                                   FileProcessorService fileProcessorService) {
        this.medicalRecordService = medicalRecordService;
        this.fileProcessorService = fileProcessorService;
    }

    /**
     * <p>This route allows users to upload a data batch.</p>
     * <p>behaviour: uploaded file is added to the db.
     * If there are duplicated records checked by code as unique key, an exception will be thrown and no change with me appeared the DB.</p>
     * <p>maximum upload size is set to 128KB.</p>
     *
     * @param file a csv file in a pre-agreed format
     * @return {@link ResponseDTO} with a default message
     */
    @RequestMapping(value = "/upload-file", method = RequestMethod.POST)
    public ResponseEntity<ResponseDTO<List<MedicalRecordDTO>>> submit(
        @RequestParam("file") MultipartFile file)
        throws FileFormatException, IOException {

        List<MedicalRecord> medicalRecords = fileProcessorService.process(file);
        List<MedicalRecord> saved = medicalRecordService.saveAll(medicalRecords);

        ResponseDTO<List<MedicalRecordDTO>> res = new ResponseDTO<>();
        res.setMessage("File uploaded successfully");
        res.setData(saved.stream().map(MedicalRecord::toDto).collect(Collectors.toList()));
        return new ResponseEntity<>(res, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<ResponseDTO<Page<MedicalRecordDTO>>> findAll(
        Pageable pageable
    ) {
        ResponseDTO<Page<MedicalRecordDTO>> res = new ResponseDTO<>();
        res.setMessage("Medical record fetched successfully");
        res.setData(medicalRecordService.findAll(pageable).map(MedicalRecord::toDto));
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @GetMapping("/{code}")
    public ResponseEntity<ResponseDTO<MedicalRecordDTO>> findByCode(
        @PathVariable("code") String code
    ) throws MedicalRecordNotFoundException {
        ResponseDTO<MedicalRecordDTO> res = new ResponseDTO<>();
        res.setMessage("Medical record fetched successfully");
        res.setData(medicalRecordService.findByCode(code).toDto());
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<ResponseDTO<MedicalRecordDTO>> deleteAll() {
        medicalRecordService.deleteAll();

        ResponseDTO<MedicalRecordDTO> res = new ResponseDTO<>();
        res.setMessage("Medical records deleted successfully");

        return new ResponseEntity<>(res, HttpStatus.OK);
    }

}
