package com.ahmad.medica.service;

import com.ahmad.medica.model.entity.MedicalRecord;
import com.ahmad.medica.model.exception.MedicalRecordNotFoundException;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MedicalRecordService {
    List<MedicalRecord> saveAll(List<MedicalRecord> medicalRecords);
    Page<MedicalRecord> findAll(Pageable pageable);
    MedicalRecord findByCode(String code) throws MedicalRecordNotFoundException;
    void deleteAll();
}
