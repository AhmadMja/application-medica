package com.ahmad.medica.service;

import com.ahmad.medica.model.entity.MedicalRecord;
import com.ahmad.medica.model.exception.FileFormatException;
import java.io.IOException;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public interface FileProcessorService {

    List<MedicalRecord> process(MultipartFile file) throws FileFormatException, IOException;

    void checkHeaders(List<String> headers) throws FileFormatException;

}
