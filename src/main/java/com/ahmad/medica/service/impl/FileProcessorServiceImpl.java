package com.ahmad.medica.service.impl;

import com.ahmad.medica.model.entity.MedicalRecord;
import com.ahmad.medica.model.exception.FileFormatException;
import com.ahmad.medica.service.FileProcessorService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Service
public class FileProcessorServiceImpl implements FileProcessorService {

    @Override
    public List<MedicalRecord> process(MultipartFile file) throws FileFormatException, IOException {
        BufferedReader fileReader = new BufferedReader(
            new InputStreamReader(file.getInputStream(),
                StandardCharsets.UTF_8)
        );

        CSVFormat format = CSVFormat.DEFAULT.builder()
            .setDelimiter(',')
            .setHeader()
            .setSkipHeaderRecord(true)  // skip header
            .build();

        CSVParser csvParser = new CSVParser(fileReader, format);

        this.checkHeaders(csvParser.getHeaderNames());

        Iterable<CSVRecord> csvRecords = csvParser.getRecords();

        List<MedicalRecord> records = new ArrayList<>();

        for (CSVRecord csvRecord : csvRecords) {
            MedicalRecord record = new MedicalRecord();
            try {
                record.setCode(csvRecord.get("code").trim());
                record.setSource(csvRecord.get("source").trim());
                record.setCodeListCode(csvRecord.get("codeListCode").trim());
                record.setDisplayValue(csvRecord.get("displayValue").trim());
                record.setLongDescription(csvRecord.get("longDescription").trim());

                record.setFromDate(csvRecord.get("fromDate").trim().length() > 0 ?
                    LocalDate.parse(csvRecord.get("fromDate"),
                        DateTimeFormatter.ofPattern("dd-MM-yyyy")) : null);

                record.setToDate(csvRecord.get("toDate").trim().length() > 0 ?
                    LocalDate.parse(csvRecord.get("toDate"),
                        DateTimeFormatter.ofPattern("dd-MM-yyyy")) : null);

                record.setSortingPriority(csvRecord.get("sortingPriority").trim().length() > 0 ?
                    Integer.parseInt(csvRecord.get("sortingPriority")) : null);
            } catch (Exception e) {
                throw new FileFormatException();
            }
            records.add(record);
        }

        return records;
    }

    public void checkHeaders(List<String> headers) throws FileFormatException {
        if (headers.size() < 8 ||
            !new HashSet<>(headers).containsAll(
                Arrays.asList("code", "source", "codeListCode", "displayValue", "longDescription",
                    "fromDate", "toDate", "sortingPriority"))) {
            throw new FileFormatException();
        }
    }

}
