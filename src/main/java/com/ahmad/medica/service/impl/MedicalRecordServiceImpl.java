package com.ahmad.medica.service.impl;

import com.ahmad.medica.model.entity.MedicalRecord;
import com.ahmad.medica.model.exception.MedicalRecordNotFoundException;
import com.ahmad.medica.model.repository.MedicalRecordRepository;
import com.ahmad.medica.service.MedicalRecordService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MedicalRecordServiceImpl implements MedicalRecordService {

    private final MedicalRecordRepository medicalRecordRepository;

    @Autowired
    public MedicalRecordServiceImpl(MedicalRecordRepository medicalRecordRepository) {
        this.medicalRecordRepository = medicalRecordRepository;
    }

    @Override
    public List<MedicalRecord> saveAll(List<MedicalRecord> medicalRecords) {
        return medicalRecordRepository.saveAll(medicalRecords);
    }

    @Override
    public Page<MedicalRecord> findAll(Pageable pageable) {
        return medicalRecordRepository.findAll(pageable);
    }

    @Override
    public MedicalRecord findByCode(String code) throws MedicalRecordNotFoundException {
        return medicalRecordRepository.findByCode(code)
            .orElseThrow(MedicalRecordNotFoundException::new);

    }

    @Override
    public void deleteAll() {
        medicalRecordRepository.deleteAll();
    }
}
