package com.ahmad.medica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationMedicaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationMedicaApplication.class, args);
	}

}
