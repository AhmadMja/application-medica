package com.ahmad.medica;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.ahmad.medica.model.entity.MedicalRecord;
import com.ahmad.medica.model.repository.MedicalRecordRepository;
import java.io.FileInputStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
class ApplicationMedicaApplicationTests {

    @Test
    void contextLoads() {
    }

    @Autowired
    private WebApplicationContext applicationContext;

    @Autowired
    private MedicalRecordRepository medicalRecordRepository;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();
    }

    @Test
    @Order(1)
    public void successOnUploadFile() throws Exception {

        FileInputStream fl = new FileInputStream("exercise.csv");
        MockMultipartFile
            file =
            new MockMultipartFile("file",
                "exercise.csv",
                "text/csv",
                fl.readAllBytes());
        fl.close();

        mockMvc.perform(
                MockMvcRequestBuilders.multipart("/data/medical-record/upload-file").file(file))
            .andExpect(status().isCreated());

        assertEquals(medicalRecordRepository.count(), 18);
    }

    @Test
    @Order(2)
    public void failOnUploadFileWithInvalidFormat() throws Exception {

        FileInputStream fl = new FileInputStream("exercise-badfile.csv");
        MockMultipartFile
            file =
            new MockMultipartFile("file",
                "exercise-badfile.csv",
                "text/csv",
                fl.readAllBytes());
        fl.close();

        mockMvc.perform(
                MockMvcRequestBuilders.multipart("/data/medical-record/upload-file").file(file))
            .andExpect(status().isUnprocessableEntity());
    }

    @Test
    @Order(3)
    public void getARecord() throws Exception {

        String code = "arandomcode";

        MedicalRecord medicalRecord = new MedicalRecord();
        medicalRecord.setCode(code);
        medicalRecordRepository.save(medicalRecord);

        mockMvc.perform(
                MockMvcRequestBuilders.get("/data/medical-record/" + code))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.data.code").value(code));
    }
}
