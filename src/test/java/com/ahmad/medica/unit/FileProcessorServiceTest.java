package com.ahmad.medica.unit;


import com.ahmad.medica.model.exception.FileFormatException;
import com.ahmad.medica.service.FileProcessorService;
import com.ahmad.medica.service.impl.FileProcessorServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class FileProcessorServiceTest {

    private final FileProcessorService fileProcessorService;

    public FileProcessorServiceTest() {
        this.fileProcessorService = new FileProcessorServiceImpl();
    }

    @Test
    public void unitTest() {
        Mockito.spy(fileProcessorService);
        List<String> fakeHeaders = Arrays.asList("col1", "col2");
        assertThrows(FileFormatException.class, () -> fileProcessorService.checkHeaders(fakeHeaders));
    }
}
