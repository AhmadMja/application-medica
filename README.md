# Documentation

This file contains brief explanation about how this application works and should be interacted with.

## Setup and Run
Run command:
```
mvn spring-boot:run
```
Run the tests:
```
mvn clean test
```

## APIs
There are 4 APIs that are implemented in this application. There is an example for each.

### Upload the data:
This api is responsible to get all data from user as a CSV file.
behaviour: uploaded file is added to the db.
* If there are duplicated records checked by code as unique key, an exception will be thrown and no change with me appeared the DB.
* If the file is not in the right format, i.e. a column is missing, a HTTP 422 will be returned.
* A list of all saved records will be returned in response.

example:
```
curl --location --request POST 'http://localhost:8080/data/medical-record/upload-file' \
--form 'file=@"/home/ahmad/Desktop/exercise.csv"'
```

### Fetch all data
This api is responsible to read all the currently saved data in DB.
* supports standard JPA pagination.
```
curl --location --request GET 'http://localhost:8080/data/medical-record?size=2&skip=1'
```

### Delete all data
This api deletes all currently saved data in the DB.

```
curl --location --request DELETE 'http://localhost:8080/data/medical-record'
```

### Get record by code
This api returns a record by code
* If there is no record with presented code, an HTTP 404 will be returned.
```
curl --location --request GET 'http://localhost:8080/data/medical-record/271636001'
```